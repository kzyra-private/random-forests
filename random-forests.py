"""
========================================================================================
    RANDOM FORESTS

    Proper way to execute a script: python3 ./random-forests.py forest_size tree_depth k_param
<int> forest_size -> number of trees in the forest
<int> tree_depth -> depth of decision tree
<int> k_param -> k-fold parameter

========================================================================================
"""

import sys
import copy
import time
import secrets
import collections
import numpy as np
import pandas as pd
import statistics as stats
from scipy.stats import entropy

pd.options.mode.chained_assignment = None

"""
========================================================================================
CHECK_ARGS -> checking correctness of passed arguments
========================================================================================
"""


def check_args():
    if len(sys.argv) != 4:
        print('Error: Invalid number of arguments. Try: python3 ./random-forests.py forest_size tree_depth k_param')
        print('Note that all passed arguments should be numbers.')
        sys.exit()

    arg_1 = int(sys.argv[1])
    arg_2 = int(sys.argv[2])
    arg_3 = int(sys.argv[3])

    if arg_1 < 0 or arg_2 < 0 or arg_3 < 2:
        print('Error: Invalid value of argument.')
        sys.exit()


"""
========================================================================================
PREPARE_SETS -> preparing training and test sets
========================================================================================
"""


def prepare_sets(dataset_, k_param):
    print("Input set will be divided into {} parts".format(k_param))
    elements = int(len(dataset_)/k_param)
    print('PREPARE SETS: elements = {}'.format(elements))

    valid = dataset_.sample(n=elements)
    dataset_ = dataset_.drop(valid.index)
    train = dataset_

    valid.reset_index(drop=True, inplace=True)
    train.reset_index(drop=True, inplace=True)

    valid.insert(len(columns), 'Predicted class', value=None)
    train.insert(len(columns), 'Predicted class', value=None)

    return [train, valid]


"""
========================================================================================
PREPARE_TESTS -> divide training set into subsets for testing
========================================================================================
"""


def prepare_tests(trainset, k_param):
    tests = []

    temp_train = trainset
    elements = int(len(trainset) / (k_param - 1))

    while len(temp_train) >= elements:
        temp_test = temp_train.sample(n=elements)
        temp_train = temp_train.drop(temp_test.index)
        temp_test.reset_index(drop=True, inplace=True)
        tests.append(temp_test)

    return tests


"""
========================================================================================
GET_ENTROPY -> calculate entropy for choosen attribute
========================================================================================
"""


def get_entropy(dataset_, at):
    probs = []
    data_freq = dataset_[at].value_counts()

    data_freq_list = data_freq.tolist()
    sum_ = sum(data_freq_list)

    for element in data_freq_list:
        probs.append(element/sum_)

    entropy_ = entropy(probs, base=2)

    return entropy_


"""
========================================================================================
CHOOSE_ATTS -> choosing attributes using entropy
========================================================================================
"""


def choose_atts_entropy(dataset_, atts):
    entropies = {}
    atts_ = []

    for at in atts:
        entropies[at] = get_entropy(dataset_, at)

    entropies_1 = sorted(entropies, reverse=True)

    for i in range(0, len(entropies_1)):
        atts_.append(entropies_1[i])

    return atts_


"""
========================================================================================
BUILD_TREE -> build a single decision tree 
========================================================================================
"""


def build_tree(sample_):
    atts_ = []
    att_values = []
    predicted = None

    for i in range(0, tree_depth):
        attribute = secrets.choice(attributes)
        atts_.append(attribute)
        att_values.append(sample_.loc[atts_[i]])

    temp_data = copy.deepcopy(train_set)

    for i in range(0, len(atts_)):
        temp_data = copy.deepcopy(temp_data[temp_data[atts_[i]] == att_values[i]])
        values = temp_data['Class'].value_counts()

        if len(values) != 0:
            predicted = values.idxmax()

    return predicted


def build_tree_entropy(sample_):
    atts_ = []
    att_values = []
    predicted = None

    for i in range(0, tree_depth):
        attribute = secrets.choice(attributes)
        atts_.append(attribute)

    atts_ = choose_atts_entropy(train_set, atts_)
    temp_data = copy.deepcopy(train_set)

    for i in range(0, len(atts_)):
        att_values.append(sample_.loc[atts_[i]])

    for i in range(0, len(atts_)):
        temp_data = copy.deepcopy(temp_data[temp_data[atts_[i]] == att_values[i]])
        values = temp_data['Class'].value_counts()
        predicted = values.idxmax()

    return predicted


"""
========================================================================================
RANDOM_FOREST -> create a random forest 
========================================================================================
"""


def random_forest(sample_):
    predicted_classes = []

    for i in range(0, forest_size):
        predicted_classes.append(build_tree(sample_))

    counter = collections.Counter(predicted_classes)
    predicted = max(counter, key=counter.get)

    sample_.loc['Predicted class'] = predicted

    return sample_


def random_forest_entropy(sample_):
    predicted_classes = []

    for i in range(0, forest_size):
        predicted_classes.append(build_tree_entropy(sample_))

    counter = collections.Counter(predicted_classes)
    predicted = max(counter, key=counter.get)

    sample_.loc['Predicted class'] = predicted

    return sample_


"""
========================================================================================
TRAINING -> classyfing samples from training sets
========================================================================================
"""


def training(tests_):
    acc_results = []

    start = time.perf_counter_ns()

    for subset in tests_:
        for i in range(0, len(subset)-1):
            subset.iloc[i] = random_forest(subset.iloc[i])

        acc_results.append(accuracy(subset))

    stop = (time.perf_counter_ns() - start) / (10 ** 9)

    print("\n*** TRAINING: Average accuracy: ({:.2%})".format(stats.mean(acc_results)))
    print('Elapsed time: {} s\n'.format(stop))


def training_entropy(tests_):
    acc_results = []

    start = time.perf_counter_ns()

    for subset in tests_:
        for i in range(0, len(subset)):
            subset.iloc[i] = random_forest_entropy(subset.iloc[i])

        acc_results.append(accuracy(subset))

    stop = (time.perf_counter_ns() - start) / (10 ** 9)

    print("\n*** TRAINING: Average accuracy: ({:.2%})".format(stats.mean(acc_results)))
    print('Elapsed time: {} s\n'.format(stop))


"""
========================================================================================
VALIDATE -> classyfing samples from validation sets
========================================================================================
"""


def validate(validset):
    start = time.perf_counter_ns()

    for i in range(0, len(validset)):
        validset.iloc[i] = random_forest(validset.iloc[i])

    stop = (time.perf_counter_ns() - start) / (10 ** 9)

    acc = accuracy(validset)
    print("\n*** VALIDATION: Accuracy: ({:.2%})".format(acc))
    print('Elapsed time: {} s'.format(stop))

    return validset


"""
========================================================================================
GET_CONFUSION_MATRIX -> returns a confusion matrix
========================================================================================
"""


def get_confusion_matrix(tested_):
    classes_ = []
    dim = len(classes)
    matrix_ = np.zeros([dim, dim])

    for i in range(0, len(classes)):
        classes_.append(tested_[tested_["Predicted class"] == classes[i]])

    i = 0
    for cls in classes_:
        for j in range(0, len(classes)):
            temp = cls[cls["Class"] == classes[j]]

            if len(temp.values) != 0:
                matrix_[i][j] = len(temp.values)

        i = i + 1

    return matrix_


"""
========================================================================================
RECALL -> returns an average recall ratio for classifier
========================================================================================
"""


def recall(matrix_):
    print('\n** RECALL')
    average_tpr = np.empty([0, 0])

    for i in range(0, len(classes)):
        nom = matrix_[i][i]
        denom = 0

        for j in range(0, len(classes)):
            denom = denom + matrix_[j][i]

        tpr = nom / denom
        average_tpr = np.append(average_tpr, tpr)
        print("Recall for class {}: {}/{} ({:.2%})".format(classes[i], int(nom), int(denom), tpr))


"""
========================================================================================
ACCURACY -> returns an accuracy ratio
========================================================================================
"""


def accuracy(tested):
    correct = 0

    for i in range(1, len(tested)):
        if tested['Class'].iloc[i] == tested['Predicted class'].iloc[i]:
            correct = correct + 1

    acc = correct / len(tested.values)
    return acc


"""
========================================================================================
PRECISION -> returns an average precision ratio for classifier
========================================================================================
"""


def precision(matrix_):
    print('\n** PRECISION')
    average_ppv = np.empty([0, 0])

    for i in range(0, len(classes)):
        nom = matrix_[i][i]
        denom = 0

        for j in range(0, len(classes)):
            denom = denom + matrix_[i][j]

        if denom != 0:
            ppv = nom / denom
        else:
            ppv = 0

        average_ppv = np.append(average_ppv, ppv)
        print("Precision for class {}: {}/{} ({:.2%})".format(classes[i], int(nom), int(denom), ppv))


"""
========================================================================================

========================================================================================
"""

check_args()
forest_size = int(sys.argv[1])
tree_depth = int(sys.argv[2])
k_parameter = int(sys.argv[3])

columns = ['Buying cost', 'Maintenace cost', 'Number of doors',
           'Number of seats', 'Lug boot size', 'Safety', 'Class']
dataset = pd.read_csv('car.csv', names=columns)

attributes = ['Buying cost', 'Maintenace cost', 'Number of doors',
              'Number of seats', 'Lug boot size', 'Safety']
classes = dataset['Class'].unique()

print('\n*** Available attributes: {} ***'.format(attributes))
print('*** Possible classes in dataset: {} ***\n'.format(classes))

[train_set, valid_set] = prepare_sets(dataset, k_parameter)
test_sets = prepare_tests(train_set, k_parameter)

training(test_sets)
training_entropy(test_sets)

valid_set = validate(valid_set)
confusion_matrix = get_confusion_matrix(valid_set)
recall(confusion_matrix)
precision(confusion_matrix)
