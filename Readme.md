# Random forests - regression & classification

This script shows how using Random Forests helps to classify a car evaluation based on given specification.

**Class Values:**
- unacc
- acc
- good
- vgood

**Attributes:**
- buying price: vhigh, high, med, low.
- price of the maintenance : vhigh, high, med, low.
- doors: 2, 3, 4, 5more.
- persons: 2, 4, more.
- lug boot: small, med, big.
- safety: low, med, high. 

More information about Random Forests algorithm available 
[here](https://towardsdatascience.com/understanding-random-forest-58381e0602d2).

## Requirements

    Python 3.8.8 (or higher)
    module numpy 1.19.5.
    module pandas 1.2.4.
    module matplotlib 3.3.4.

This script also requires Car Evaluation Dataset from UCI Machine Learning Repository:  
[Car Evaluation dataset](https://archive.ics.uci.edu/ml/datasets/Car+Evaluation)


## How to run

    > python3 ./random-forests.py forest_size tree_depth k_param

    Arguments:
        <int> forest_size -> number of trees in the forest
        <int> tree_depth -> depth of decision tree
        <int> k_param -> k-fold parameter


## Conclusions

Implemented classifier gives the best results for two most frequent classes: `unacc` and `acc`.

More trees in forest helps to receive more appropiate classification, but too high number may lead to accuracy decrease due to bigger randomness in a model. Increasing tree's depth always helps to get better results, even in a situation of usage one attribute more than once.